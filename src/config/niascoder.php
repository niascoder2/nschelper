<?php

return [
    'validation_bypass_nik' => env('VALIDATION_BYPASS_NIK', false),
    'validation_bypass_nokk' => env('VALIDATION_BYPASS_NOKK', false),
    'rows_per_page' => env('ROWS_PER_PAGE', 10),
    'rows_per_page_options' => env('ROWS_PER_PAGE_OPTIONS', [5, 10, 25, 50, 100]),
];
