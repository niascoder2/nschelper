<div class="recaptcha-container">
    <div class="g-recaptcha" data-sitekey="{{ $siteKey }}" id="{{ $name }}-recaptcha"></div>
    @if ($modalForm)
        <script>
            grecaptcha.render('{{ $name }}-recaptcha', {});
        </script>
    @endif
</div>
