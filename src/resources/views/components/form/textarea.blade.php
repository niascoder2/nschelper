<div class="mb-3">
    <label for="{{ $name }}" class="form-label">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <textarea
        id="{{ $name }}"
        name="{{ $name }}"
        class="form-control @if (!$disabled && !$readOnly) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif"
        @if ($placeholder) placeholder="{{ $placeholder }}" @endif
        @if ($rows) rows="{{ $rows }}" @endif
        @if ($cols) cols="{{ $cols }}" @endif
        @if ($required) required @endif
        @if ($disabled) disabled @endif
        @if ($readOnly) readonly @endif>{{ old($name, $oldValue) }}</textarea>
    @if (!$disabled && !$readOnly)
        @error($name)
            <small class="validation-error form-text d-block text-danger">{{ $message }}</small>
        @enderror
    @endif
    @if ($displayHelpText)
        <small id='{{ $name . '.' . '_help' }}' class='form-text d-block text-muted'>{{ $displayHelpText }}</small>
    @endif
</div>
