<div class="mb-3">
    <label for="{{ $name }}" class="form-label">
        {{ $displayLabel }}
        @if ($required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <select
        id="{{ $name }}"
        name="{{ $name }}"
        class="form-control form-select @if (!$disabled) @error($name) is-invalid @else @if ($errors->any()) is-valid @endif @enderror @endif"
        @if ($size) size="{{ $size }}" @endif
        @if ($required) required @endif
        @if ($disabled) disabled @endif
        {{ $attributes }}>
        <option value="">{{ $placeholder }}</option>
        @if ($options)
            @foreach ($options as $key => $value)
                <option value="{{ $key }}" {{ old($name, $oldValue) == $key ? 'selected' : '' }}>{{ $value }}</option>
            @endforeach
        @else
            {{ $slot }}
        @endif
    </select>
    @if (!$disabled)
        @error($name)
            <small class="validation-error form-text d-block text-danger">{{ $message }}</small>
        @enderror
    @endif
    @if ($displayHelpText)
        <small id='{{ $name . '.' . '_help' }}' class='form-text d-block text-muted'>{{ $displayHelpText }}</small>
    @endif
</div>
