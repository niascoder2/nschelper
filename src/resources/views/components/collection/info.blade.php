<span class="collection-info">
    {{ $dataTotal }} data.
    @if ($isTersaring)
        <strong>Tersaring.</strong>
        @if ($postUrl != '')
            <a href="{{ $postUrl }}" title="reset">(reset)</a>
        @endif
    @endif
</span>
