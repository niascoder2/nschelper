<?php

return [

    // kamus label untuk menampilkan data dari database

    // ------------------------------------------------------------
    // contoh:
    // 
    // 'pegawai.edit.nama' => 'Nama Lengkap',
    // 'pegawai.edit.nama_help' => 'Tuliskan nama lengkap pegawai',
    // 
    // maka lang-context pada component: db.pegawai.edit
    // ------------------------------------------------------------

];
