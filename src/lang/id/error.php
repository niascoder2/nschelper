<?php

// kamus message untuk menandakan error

return [

    'internal_error' => 'Ada kesalahan internal sistem.',

    'data_not_found' => 'Data tidak ditemukan.',
    'data_not_valid' => 'Data yang diterima tidak valid.',

    'operation_invalid' => 'Operasi tidak valid.',
    'operation_failed' => 'Operasi gagal.',

    'db_failed' => 'Operasi database gagal.',

    'access_denied' => 'Tidak memiliki akses.',

    'file_too_big' => 'Ukuran file terlalu besar.',
    'file_type_not_allowed' => 'Ukuran file terlalu besar.',
];
