<?php

return [

    // kamus message untuk tooltip

    'detail_button' => 'klik untuk melihat detail',
    'tambah_button' => 'klik untuk menambah',
    'edit_button' => 'klik untuk mengedit',
    'simpan_button' => 'klik untuk menyimpan',
    'hapus_button' => 'klik untuk menghapus',
    'batal_button' => 'klik untuk membatalkan',
    'kembali_button' => 'klik untuk kembali',
    'filter_button' => 'klik untuk menyaring atau mencari data',
];
