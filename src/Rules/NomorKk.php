<?php

namespace NiasCoder\LaravelHelper\Rules;

use Illuminate\Contracts\Validation\Rule;

class NomorKk implements Rule
{
    public function passes($attribute, $value)
    {
        if (config('niascoderhelper.validation_bypass_nokk')) {
            return true;
        } else {
            if (!preg_match("/^[0-9]{16}$/", $value)) return false;
            return true;
        }
    }

    public function message()
    {
        return 'validation.nomorkk';
    }
}
