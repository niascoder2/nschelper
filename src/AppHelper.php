<?php

namespace NiasCoder\LaravelHelper;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as ValidationValidator;
use Throwable;

class AppHelper
{
    static function parseTanggal(mixed $tanggal, string $format): mixed
    {
        if (is_null($tanggal)) {
            return null;
        }
        $check = date_parse_from_format($format, $tanggal);
        if ($check['warning_count'] == 0 && $check['error_count'] == 0) {
            return date_create($check['year'] . '-' . $check['month'] . '-' . $check['day']);
        } else {
            return null;
        }
    } // function parseTanggal

    static function showTanggal(mixed $tanggal, string $format): string
    {
        if (is_null($tanggal)) {
            return '';
        }
        return Carbon::parse($tanggal)->translatedFormat($format);
    } // function showTanggal

    static function showInteger(mixed $number): string
    {
        if (is_null($number) || !is_numeric($number)) {
            return '';
        } elseif ($number == 0) {
            return '-';
        } else {
            return number_format($number);
        }
    } // showInteger

    static function attrToArray(Model $object): array
    {
        if (!$object) return [];
        return $object->attributesToArray();
    } // function attrToArray

    static function validatorErrorForm(string $key, string $message): ValidationValidator
    {
        $validator = Validator::make([], []);
        $validator->getMessageBag()->add($key, $message);
        return $validator;
    } // function validatorErrorForm

    static function validatorErrorMessage(string $message): ValidationValidator
    {
        $validator = Validator::make([], []);
        $validator->getMessageBag()->add('error_alert', $message);
        return $validator;
    } // function validatorErrorMessage

    static function validatorErrorTh(Throwable $th, string $message = null): ValidationValidator
    {
        if (!$message) $message = 'Operasi database gagal.';
        if (!App::environment(['production']) && $th) {
            $message = $message . " [" . $th->getMessage() . "]";
        }
        $validator = Validator::make([], []);
        $validator->getMessageBag()->add('error_alert', $message);
        return $validator;
    } // function validatorErrorTh

    static function jsonResponseErrorMessage(string $message, string $redirect = null): array
    {
        return ['status' => false, 'desc' => $message, 'redirect' => $redirect];
    } // function jsonResponseErrorMessage

    static function jsonResponseErrorValidator(ValidationValidator $validator): array
    {
        $desc = '';
        foreach ($validator->errors()->messages() as $messages) {
            foreach ($messages as $message) {
                $desc = $desc . '<p>' . $message . '</p>';
            }
        }
        return ['status' => false, 'desc' => $desc, 'validator' => $validator->errors()->messages()];
    } // function jsonResponseErrorMessage

    static function jsonResponseErrorTh(Throwable $th, string $message = null, string $redirect = null): array
    {
        if (!$message) $message = 'Operasi database gagal.';
        if (!App::environment(['production']) && $th) {
            $message = $message . " [" . $th->getMessage() . "]";
        }
        return ['status' => false, 'desc' => $message, 'redirect' => $redirect];
    } // function jsonResponseErrorTh

    static function jsonResponseSuccess(string $redirect = null): array
    {
        return ['status' => true, 'redirect' => $redirect];
    } // function jsonResponseSuccess

} // class AppHelper
