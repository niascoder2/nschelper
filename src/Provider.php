<?php

namespace NiasCoder\LaravelHelper;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

use NiasCoder\LaravelHelper\Rules\Nik;
use NiasCoder\LaravelHelper\Rules\NomorKk;
use NiasCoder\LaravelHelper\Rules\TglBlnThn;
use NiasCoder\LaravelHelper\View\Components\Collection\Filtering;
use NiasCoder\LaravelHelper\View\Components\Collection\Info;
use NiasCoder\LaravelHelper\View\Components\Collection\RowPerPage;
use NiasCoder\LaravelHelper\View\Components\Form\File;
use NiasCoder\LaravelHelper\View\Components\Form\Input;
use NiasCoder\LaravelHelper\View\Components\Form\Recaptcha2;
use NiasCoder\LaravelHelper\View\Components\Form\Select;
use NiasCoder\LaravelHelper\View\Components\Form\Textarea;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /* ---------------------------------- views --------------------------------- */

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'niascoder-helper');

        /* ------------------------------- components ------------------------------- */

        Blade::component('form-input', Input::class);
        Blade::component('form-recaptcha2', Recaptcha2::class);
        Blade::component('form-file', File::class);
        Blade::component('form-select', Select::class);
        Blade::component('form-textarea', Textarea::class);

        Blade::component('collection-filtering', Filtering::class);
        Blade::component('collection-info', Info::class);
        Blade::component('collection-rowperpage', RowPerPage::class);

        /* -------------------------------- validator ------------------------------- */

        Validator::extend('tglblnthn', function ($attribute, $value, $parameters, $validator) {
            return (new TglBlnThn)->passes($attribute, $value);
        });
        Validator::extend('nik', function ($attribute, $value, $parameters, $validator) {
            return (new Nik)->passes($attribute, $value);
        });
        Validator::extend('nomorkk', function ($attribute, $value, $parameters, $validator) {
            return (new NomorKk)->passes($attribute, $value);
        });

        /* --------------------------------- publish -------------------------------- */

        $this->publishes([
            __DIR__ . '/config/niascoder.php' => config_path('niascoder.php'),
            __DIR__ . '/lang' => lang_path(),
            __DIR__ . '/public' => public_path(),
        ], 'niascoder-helper');
    }
}
