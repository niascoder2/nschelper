<?php

namespace NiasCoder\LaravelHelper\View\Components\Form;

use Illuminate\View\Component;

class Select extends Component
{

    public string $name;
    public string $langContext;
    public string $placeholder;
    public string $helpText;
    public int $size;

    public bool $required;
    public bool $disabled;

    public mixed $options;
    public mixed $oldValue;

    public string $displayLabel;
    public string $displayHelpText;

    public function __construct($name, $langContext, $placeholder = '', $helpText = '', $size = 0, $required = false, $disabled = false, $options = null, $oldValue = null)
    {
        $this->name = $name;
        $this->langContext = $langContext;
        $this->placeholder = $placeholder;
        $this->helpText = $helpText;
        $this->size = $size;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->options = $options;
        $this->oldValue = $oldValue;

        $this->displayLabel = __($langContext . "." . $name);
        if (!$helpText || $helpText == '') {
            $context = $langContext . "." . $name . "_help";
            $helpText = __($context);
            if ($helpText != $context) {
                $this->displayHelpText = __($langContext . "." . $name . "_help");
            } else {
                $this->displayHelpText = '';
            }
        } else {
            $this->displayHelpText = $helpText;
        }
    }

    public function render()
    {
        return view('niascoder-helper::components.form.select');
    }
}
